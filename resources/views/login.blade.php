<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    @include ('style')
    @include ('script')
    <title>Document</title>
   
</head>
<body>
    <div class="container">
        <div class ="row">
            <div class="col-sm-4 col-md-4 col-lg-4 col-xl-4"></div>
            <div class="col-sm-4 col-md-4 col-lg-4 col-xl-4 border">
                <form action="login" method="POST">
                @csrf
                    <h2 class="text-center">Login </h2>
                    <div class="form-group" >
                        <input type="text" name="username" class="form-control" placeholder="Enter email">
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" class="form-control" placeholder="Enter password">
                    </div>
                    <div  class="form-group">
                        <input type="submit" class=" btn btn-primary" value ="Login">
                    </div>
                    <div class="float-sm-left">forget</div>
                    <div class="float-sm-right">register</div>
                </form>
            </div>
            <div class="col-sm-4 col-md-4 col-lg-4 col-xl-4"></div>
        </div>
         
       
    </div>
        

            
</body>
</html>